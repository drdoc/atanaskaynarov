﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using ProjectManager.Enumerations;
using ProjectManager.Interfaces;

namespace ProjectManager
{
    public interface ITask : IEntity
    {
        
        string Description { get; set; }
        int CreatorId { get; set; }
        List<int> DevelopersId { get; set; }
        List<int> LogsId { get; set; }
        bool IsCompleted { get; set; }

    }
}
