﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Interfaces
{
    public interface ILog : IEntity
    {
        int WorkedHours { get; set; }
        int CreatorId { get; set; }
        string Comment { get; set; }

    }
}
