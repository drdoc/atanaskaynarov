﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Interfaces
{
    interface IController<T, R> 
        where T : IEntity
    {
        List<T> GetAll();
        void Save(T item);
        void DeleteById(int id);
        T GetById(int id);
    }
}
