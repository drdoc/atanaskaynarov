﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ProjectManager.Interfaces;

namespace ProjectManager
{
    public interface IRepository<T> where T : IEntity
    {
        int GetNextId();
        void Insert(T user);
        void Update(T user);
        void Delete(T user);
        T GetById(int id);
        List<T> GetAll();
        List<T> GetAll(int id); 
        void Save(T user);
    }
}
