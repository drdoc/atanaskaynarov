﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Tools;

namespace ProjectManager.Interfaces
{
    public interface IEngine
    {
        bool HasStarted { get; set; }
        bool IsLogged { get; }
        IUser CurrentUser { get; set; }
        void Run();
    }
}
