﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectManager.Interfaces;

namespace ProjectManager
{
    public interface IUser : IEntity

    {
        string Username { get; set; }

        string Password { get; set; }

        string FirstName { get; set; }
        string LastName { get; set; }

            

    }
}
