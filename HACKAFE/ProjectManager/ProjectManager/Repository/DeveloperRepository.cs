﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ProjectManager.Interfaces;

namespace ProjectManager
{
    public class DeveloperRepository: Repository<Developer>
    {   
        public DeveloperRepository()
        {
            FilePath = "DeveloperRepository.txt";
        }

        //Lines of information

        //id
        //username
        //password
        //firstname
        //lastname
        protected override void ObjectWriter(Developer developer, StreamWriter sw)
        {
            sw.WriteLine(developer.Id);
            sw.WriteLine(developer.Username);
            sw.WriteLine(developer.Password);
            sw.WriteLine(developer.FirstName);
            sw.WriteLine(developer.LastName);
        }
        protected override Developer ObjectReader(StreamReader sr)
        {
            Developer developer = new Developer();
            developer.Id = int.Parse(sr.ReadLine());
            developer.Username = sr.ReadLine();
            developer.Password = sr.ReadLine();
            developer.FirstName = sr.ReadLine();
            developer.LastName = sr.ReadLine();
            return developer;
        }

       
    }
}
