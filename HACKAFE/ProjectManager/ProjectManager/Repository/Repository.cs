﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using ProjectManager.Interfaces;

namespace ProjectManager
{   

    
    public abstract class Repository<T> : IRepository<T> 
        where T : IEntity, new()
    {
        //Property, which will be changed in every kind of repository
        public string FilePath { get; set; }

        public virtual int GetNextId()
        {
            int freeId = 1;
            T entityObject = new T();

            using (FileStream fs = new FileStream(FilePath, FileMode.OpenOrCreate))
            using (StreamReader sr = new StreamReader(fs))
            {
                while (!sr.EndOfStream)
                {
                    entityObject = ObjectReader(sr);
                    if (freeId <= entityObject.Id)
                    {
                        freeId = entityObject.Id + 1;
                    }
                }
            }


            return freeId;

        }

        public  void Insert(T entityObject)
        {
            entityObject.Id = GetNextId();
            using (FileStream fs = new FileStream(FilePath, FileMode.Append))
            using (StreamWriter sr = new StreamWriter(fs))
            {
                ObjectWriter(entityObject, sr);
            }
        }



      


        public  List<T> GetAll()
        {
            List<T> objectsList = new List<T>();
            try
            {
                using (FileStream fs = new FileStream(FilePath, FileMode.OpenOrCreate))
                using (StreamReader sr = new StreamReader(fs))
                {
                    while (!sr.EndOfStream)
                    {
                        var entityObject = ObjectReader(sr);

                        objectsList.Add(entityObject);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return objectsList;

        }

        public List<T> GetAll(int id)
        {
            List<T> list = GetAll();
            return list.FindAll(x => x.Id == id);
        } 

        public  void Save(T entityObject)
        {
            if (entityObject.Id > 0)
            {
                Update(entityObject);
            }
            else
            {
                Insert(entityObject);
            }
        }

        public  void Update(T user)
        {
            string tempFilePath = "temp." + FilePath;
            FileStream newFS = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(newFS);

            FileStream oldFS = new FileStream(FilePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(oldFS);

            try
            {
                while (!sr.EndOfStream)
                {
                    T entityObject = ObjectReader(sr);

                    if (entityObject.Id == user.Id)
                    {
                        ObjectWriter(user, sw);
                    }
                    else
                    {
                        ObjectWriter(entityObject, sw);
                    }

                }

                
            }

            finally
            {
                sr.Close();
                sw.Close();
                newFS.Close();
                oldFS.Close();
                
            }

            File.Delete(FilePath);
            File.Move(tempFilePath, FilePath);

        }

        public  void Delete(T user)
        {
            string tempFilePath = "temp." + FilePath;
            FileStream newFS = new FileStream(tempFilePath, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(newFS);

            FileStream oldFS = new FileStream(FilePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(oldFS);

            try
            {
                while (!sr.EndOfStream)
                {
                    T entityObject = ObjectReader(sr);

                    if (entityObject.Id == user.Id)
                    {
                        continue;
                    }
                    else
                    {
                        ObjectWriter(entityObject, sw);
                    }

                }

            }

            finally
            {
                sr.Close();
                sw.Close();
                newFS.Close();
                oldFS.Close();
                
            }


            File.Delete(FilePath);
            File.Move(tempFilePath, FilePath);
        }

        public  T GetById(int id)
        {
            FileStream fs = new FileStream(FilePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    T entityObject = ObjectReader(sr);
                    if (entityObject.Id == id)
                    {
                        return entityObject;
                    }
                }

                throw new Exception("User not found");

            }
            catch (Exception ex )
            {
                Console.WriteLine(ex.Message);
                return default(T);

            }
            finally
            {
                fs.Close();
            }


        }

        protected virtual T ObjectReader(StreamReader sr)
        {
            throw new NotImplementedException();
        }

        protected  virtual void ObjectWriter(T entityObject, StreamWriter sw)
        {
            throw new NotImplementedException();

        }
    }
}
