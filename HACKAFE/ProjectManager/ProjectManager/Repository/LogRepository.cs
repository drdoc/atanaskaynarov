﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Entity;
using ProjectManager.Interfaces;

namespace ProjectManager
{
    public class LogRepository : Repository<Log>
    {
        public LogRepository()
        {
            FilePath = "LogRepository.txt";
        }

        //Lines of information

        //id
        //WorkedHours
        //CreatorID
        //Comment

        protected override void ObjectWriter(Log entityObject, StreamWriter sw)
        {
            sw.WriteLine(entityObject.Id);
            sw.WriteLine(entityObject.WorkedHours);
            sw.WriteLine(entityObject.CreatorId);
            sw.WriteLine(entityObject.Comment);
            sw.WriteLine(entityObject.TaskId);

        }
        protected override Log ObjectReader(StreamReader sr)
        {
            Log log = new Log();
            log.Id = int.Parse(sr.ReadLine());
            log.WorkedHours = int.Parse(sr.ReadLine());
            log.CreatorId = int.Parse(sr.ReadLine());
            log.Comment = sr.ReadLine();
            log.TaskId = int.Parse(sr.ReadLine());
            return log;
        }
    }
}
