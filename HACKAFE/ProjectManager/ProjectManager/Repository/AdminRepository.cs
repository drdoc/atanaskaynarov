﻿using ProjectManager.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ProjectManager
{
    public class AdminRepository : Repository<Admin>
    {   
        

        
        public AdminRepository()
        {
            FilePath = "AdminRepository.txt";
        }

        //Lines of information

        //id
        //username
        //password
        //firstname
        //lastname

        protected override  void ObjectWriter(Admin entityObject, StreamWriter sw)
        {
            sw.WriteLine(entityObject.Id);
            sw.WriteLine(entityObject.Username);
            sw.WriteLine(entityObject.Password);
            sw.WriteLine(entityObject.FirstName);
            sw.WriteLine(entityObject.LastName);
        }

        protected override Admin ObjectReader(StreamReader sr)
        {
            Admin entityObject = new Admin();
            entityObject.Id = int.Parse(sr.ReadLine());
            entityObject.Username = sr.ReadLine();
            entityObject.Password = sr.ReadLine();
            entityObject.FirstName = sr.ReadLine();
            entityObject.LastName = sr.ReadLine();
            return entityObject;
        }
    }
}
