﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Repository
{
    public class TaskRepository : Repository<Task>
    {
        public TaskRepository()
        {
            this.FilePath = "TaskRepository.txt";
        }
        //Id
        //CreatorId
        //DevelopersId
        //LogsId
        //Description
        protected override Task ObjectReader(StreamReader sr)
        {
            Task task = new Task();

            task.Id = int.Parse(sr.ReadLine());
            task.CreatorId = int.Parse(sr.ReadLine());
            task.DevelopersId = sr.ReadLine().Split(' ').Select(int.Parse).ToList();
            var readLine = sr.ReadLine();
            if (readLine != "") task.LogsId = readLine.Split(' ').Select(int.Parse).ToList();
            task.Description = sr.ReadLine();
            task.IsCompleted = sr.ReadLine() == "True" ? true : false;

            return task;
        }

        protected override void ObjectWriter(Task entityObject, StreamWriter sw)
        {
            string[] developersId = entityObject.DevelopersId.Select(x => x.ToString()).ToArray();
            string[] logsId = entityObject.LogsId.Select(x => x.ToString()).ToArray();

            sw.WriteLine(entityObject.Id);
            sw.WriteLine(entityObject.CreatorId);
            sw.WriteLine(String.Join(" ", developersId));
            sw.WriteLine(String.Join(" ", logsId));
            sw.WriteLine(entityObject.Description);
            sw.WriteLine(entityObject.IsCompleted.ToString()); 
        }
    }
}
