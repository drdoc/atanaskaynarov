﻿using ProjectManager.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Tools
{
    public  class TasksController : BaseController<Task, TaskRepository>
    {

        internal void Complete(int id)
        {
            Task task = this.GetById(id);
            task.IsCompleted = true;
            if (AuthenticationService.LoggedUser.Id == task.CreatorId)
            {
                Save(task);
                Console.WriteLine("Sucessfully completed");
            }
            else
            {
                Console.WriteLine("Can not complete task. You are not the creator of the task!!!");
            }
        }
    }
}
