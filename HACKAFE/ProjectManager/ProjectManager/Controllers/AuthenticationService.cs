﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager;
using ProjectManager.Interfaces;

namespace ProjectManager.Tools
{
    public static class AuthenticationService
    {
        public static User LoggedUser { get; private set; }

        public static bool IsLogged
        {
            get { return AuthenticationService.LoggedUser != null; }
        }

        public static void Authenticate(string username, string password)
        {   

           
                AdminRepository ar = new AdminRepository();
                DeveloperRepository dr = new DeveloperRepository();

                List<Admin> admins = ar.GetAll();
                List<Developer> developers = dr.GetAll();

                User user = admins.FirstOrDefault(x => x.Username == username && x.Password == password);
                if (user == null)
                {
                    user = developers.FirstOrDefault(x => x.Username == username && x.Password == password);
                }

                AuthenticationService.LoggedUser = user;

               
            
        }

        public static void Register(Developer developer)
        {
            
            try
            {
                CheckUsername(developer.Username);

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            
           DevelopersController dc = new DevelopersController();
            dc.Save(developer);

        }

        private static void CheckUsername(string username)
        {
            DeveloperRepository dr = new DeveloperRepository();
            var list = dr.GetAll();
            if (list.Any(developer => developer.Username == username))
            {
                throw new Exception("Username already taken");
            }

        }
    }

    
}

   