﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Entity;

namespace ProjectManager.Tools
{
    public class LogsController : BaseController<Log, LogRepository>
    {
        public  void Save(Log item, int taskId)
        {
            TasksController tc = new TasksController();
            Task task = tc.GetById(taskId);

            if (AuthenticationService.LoggedUser.Id == task.CreatorId ||
                task.DevelopersId.Any(d => d == AuthenticationService.LoggedUser.Id))
            {   

                Save(item);
                //Update the task, so that the 
                Console.WriteLine("Sucessfully logged!");
                Console.ReadKey(true);
            }
            //error
            else
            {
                Console.WriteLine("You dont have permissions to log there");
                Console.ReadKey(true);
            }
        }
    }
}
