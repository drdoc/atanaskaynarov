﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Interfaces;

namespace ProjectManager.Tools
{
    public abstract class BaseController<T, R> : IController<T, R>
        where T : IEntity, new()
        where R : IRepository<T> ,new()
    {
        public  List<T> GetAll()
        {
            R repo = new R();
            List<T> items = repo.GetAll();
            return items;
        }

        public virtual void Save(T item)
        {
            R repo = new R();
            repo.Save(item);
        }

        public  void DeleteById(int id)
        {
            R repo = new R();
            repo.Delete(repo.GetById(id));
        }
        public T GetById(int id)
        {
            R repo = new R();
            return repo.GetById(id);
        }

        public List<T> GetAll(int id)
        {
            R repo = new R();
            return repo.GetAll(id);
        }
    }
}
