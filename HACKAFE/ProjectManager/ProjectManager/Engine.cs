﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Interfaces;
using ProjectManager.Tools;
using ProjectManager.View;

namespace ProjectManager
{
    public class Engine : IEngine
    {
        private const string DefaultAdminUser = "admin";
        private const string DefaultAdminPassword = "admin";
        private const string DefaultAdminFirstName = "admin";
        private const string DefaultAdminLastName = "admin";
        //This is the command module for the engine
       

        public Engine()
        {
            this.HasStarted = true;
        }
        public bool HasStarted { get; set; }

        public bool IsLogged
        {
            get { return this.CurrentUser != null; }
        }

        public IUser CurrentUser { get; set; }
        public ITask CurrentTask { get; set; }

        public void Run()
        {
            SetUp();
            ExecuteCommandLoop();
        }

        private void ExecuteCommandLoop()
        {
            while (this.HasStarted)
            {
                if (!AuthenticationService.IsLogged)
                {
                   WelcomeView wv = new WelcomeView();
                    wv.Show();
                   
                }
                else
                {
                    HomeView cv = new HomeView(this);
                    cv.Show();
                }
                
            }
        }

        protected virtual void SetUp()
        {
            AdminRepository ar = new AdminRepository();
            List<Admin> list = ar.GetAll();
            if (list.Count == 0)
            {
                Admin admin = new Admin();
                admin.FirstName = DefaultAdminFirstName;
                admin.LastName = DefaultAdminLastName;
                admin.Username = DefaultAdminUser;
                admin.Password = DefaultAdminPassword;
                ar.Insert(admin);
            }
        }

        //ENGINE COMMANDS
        


    }
}
