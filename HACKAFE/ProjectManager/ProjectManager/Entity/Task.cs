﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectManager.Entity;
using ProjectManager.Enumerations;
using ProjectManager.Interfaces;
using ProjectManager.Tools;

namespace ProjectManager
{
    public class Task : ITask
    {
        public Task()
        {
            this.DevelopersId = new List<int>();
            this.LogsId = new List<int>();
        }
        public int Id { get; set; }

        public string Description { get; set; }

        public int CreatorId { get; set; }


        public List<int> DevelopersId { get; set; }

        public List<int> LogsId { get; set; }


        public bool IsCompleted  { get; set; }

        public override string ToString()
        {
            DevelopersController dc = new DevelopersController();
            TasksController tc = new TasksController();
            LogsController lc = new LogsController();

            Developer  creator = dc.GetById(this.CreatorId);

            List<Log> logs = lc.GetAll().FindAll(l => l.TaskId == this.Id);
            List<Developer> developers = dc.GetAll().FindAll(l => DevelopersId.Any(x => x == l.Id));

            StringBuilder output = new StringBuilder();

            output.AppendLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            output.AppendLine("ID: " + this.Id);
            output.AppendLine("Task: " + this.Description);
            output.AppendLine("Creator: " + creator.Username);
            output.AppendLine("Completed: " + this.IsCompleted);
            output.AppendLine("------Working Users-----");

            foreach (Developer developer in developers)
            {
                output.AppendLine(developer.ToString());
            }

            output.AppendLine("-----LOGS------");

            foreach (Log log in logs)
            {
                output.AppendLine(log.ToString());
            }

            output.AppendLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


            return output.ToString().Trim();
        }
    }
}
