﻿using ProjectManager.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectManager.Tools;

namespace ProjectManager.Entity
{
    public class Log : ILog
    {
        public int Id { get; set; }
        public int WorkedHours { get; set; }
        public int CreatorId { get; set; }
        public string Comment { get; set; }
        public int TaskId { get; set; }

        public override string ToString()
        {   
            DevelopersController dc = new DevelopersController();
            Developer developer = dc.GetById(this.CreatorId);
            return String.Format("Username: '{0}' Worked hours: {1}, Comment: '{2}'", developer.Username, this.WorkedHours, this.Comment );
        }
    }
}
