﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManager.Enumerations
{
    public enum TaskStatusEnum
    {
        Approved,
        Completed,
        Uncompleted

    }
}
